
const todoPanel = document.querySelector(".todo__panel");
const btnAdd = document.querySelector(".todo__btn_add");
const btnCompleted = document.querySelector(".todo__btn_completed");
const btnClear = document.querySelector(".todo__btn_clear");
const todoList = document.querySelector(".todo__list");
const taskCompleted = "todo__list_item_check--completed";
    
btnAdd.addEventListener("click", () => {
    if(!todoPanel.value == "") {
        let todoListItem = document.createElement("li");
              todoListItem.className = "todo__list_item";
        let todoListItemCheck = document.createElement("button");
              todoListItemCheck.className = "todo__list_item_check";
        let todoListItemBtnClose = document.createElement("button");
              todoListItemBtnClose.className = "todo__list_item_btn";
              todoListItemBtnClose.innerHTML = "<i class='fas fa-times'>";
        
        todoListItemCheck.innerHTML = todoPanel.value;
        todoListItem.appendChild(todoListItemCheck);
        todoListItem.appendChild(todoListItemBtnClose);
        todoList.appendChild(todoListItem);
    } 

    todoPanel.value = "";  
});

todoList.addEventListener("click", (e) => {
    if(e.target.parentElement.classList.contains("todo__list_item_btn")) {
        e.target.parentElement.parentElement.remove();
    }
});

todoList.addEventListener("click", (e) => {
    if(e.target.parentElement.classList.contains("todo__list_item") ) {
        e.target.parentElement.classList.toggle(taskCompleted);
    }
});

btnClear.addEventListener("click", () => {
    todoList.innerHTML = "";
});

btnCompleted.addEventListener("click", () => {
    for(let i = 0; i < todoList.childNodes.length; i++) {
        if(todoList.childNodes[i].classList.contains(taskCompleted)) {
            todoList.childNodes[i].className = "hidden";
        }
    }
});






